var ilet={
	odd:function(x){return !!(x%2)},
	even:function(x){return !ilet.odd(x)},
	t:function(x){return Object.prototype.toString.call(x).slice(8,-1)},
	a:function(a){return Array.apply(0,a)},
	b:function(o){return Boolean(o)},
	s:function(x){return x+[]},
	i:function(x){return Math.floor(x)},
	N:function(n){return !isNaN(n)},
	B:function(x){return ilet.T(x, "Boolean")},
	A:function(x){return null!==ilet.sa(x)},
	S:function(x){return ilet.T(x, "String")},
	F:function(x){return ilet.T(x, "Function")},
	T:function(x,y){return y == ilet.t(x)},
	F:function(x){return x instanceof Function},
	validDate:function(d){return "Invalid Date"!=new Date(d)},
	time:function(){return new Date/1e3},
	sa:function(a){try{return ilet.a(a)}catch(e){return null}},
	chr:function(x){return String.fromCharCode(x);},
	ord:function(x){return x.charCodeAt();},
	_12:function(){var d="";for(var i=0;i<10;i++)d+=i;return d},
	ABC:function(){var d="";for(var i=65;i<91;i++)d+=ilet.chr(i);return d},
	abc:function(){var d="";for(var i=97;i<123;i++)d+=ilet.chr(i);return d},
	alpha:function(){return ilet.abc()},
	Alpha:function(){return ilet.ABC()},
	iof:function(x, y){x instanceof y},
	uni:function(arr, wS){
		if (ilet.S(arr))
			return ilet.uni(arr.split(""), 1);
		var r = [];
		if (ilet.A(arr))
			for (var i in arr)
				if (!ilet.inA(r, arr[i]))
					r.push(arr[i]);
		if (wS) return r.join("");
		return r;
	}, inA:function(arr, I, r){
		r = 0;
		for (var i in arr)
			if (arr[i] == I)
				r = 1;
		return !!r;
	}, params: function(urlSearch, iO){
		var a, b, p, pairs = [], r = urlSearch.split("&");
		for (var i in r)
		{
			a = decodeURI((p = r[i].split("="))[0]), b = decodeURI(p[1]||"");
			if (iO) pairs.push({key:a, val:b});
			else pairs.push([a, b]);
		}
		return pairs;
	},
	Timer: function(cf){
		this.on = function(x, f){cf[x]=f;return this};
		this.tick = function(f){cf.tick=f};
		this.done = function(f){cf.done=f};
		this.start = init;
		this.end = stop;
		function stop(){tmr=clearInterval(tmr)}
		if (!ilet.T(cf, "Object"))
			cf = {};
		cf.nap = cf.nap || 1e3;
		var tmr, ticks = 0;
		function init(){
			if (tmr) return;
			tmr = setInterval(function(){
				if (ilet.F(cf.tick))
					cf.tick(ticks);
				if (cf.till && ticks >= cf.till)
					if (stop(), ilet.F(cf.done))
						cf.done(ticks);
				ticks++;
			}, cf.nap);
		}
		if (cf.play || cf.autostart) init();
	}
};